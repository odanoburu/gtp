{-|
Module      : Graph.ITP
Description : Interactive graph theorem prover
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declare functions for interactive proofs.
-}

{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes #-}
module Graph.ITP (
  -- * Data types used to provide information to the REST API server
  --
  -- | These data types are not exact representations of the data in
  -- the backend; they are closer to how the frontend sees them.
  NodeKind(..),
  ProofNode(..),
  ReqResult(..),
  checkProofs,
  deleteProof,
  goalHypotheses,
  implicationElimBackwardInteractive,
  implicationIntroBackwardInteractive,
  loadRoot,
  proofTree,
  revertProof,
  showRoots,
    ) where

import Graph.Lib (App)
import Graph.Bolt ((=!), Bolt, FormulaId(..), HypothesisDiscard(..)
                  , GoalId(..), ElimRes(..), IntroRes(..), Label
                  , NodeId(..), NodeKind(..), RootId(..)
                  , bolt, boltTransact, collectHypotheses, deleteSubproof
                  , derives
                  , discardHypothesisCloseBranch, getCrownFromRoot
                  , implicationElimBackward, implicationIntroBackward
                  , loadRootFormula, maybeResult, nodeKind, oneResult
                  , revertSubproof)

import Control.Monad (msum)
import Data.Aeson ( ToJSON(..), Options(..), defaultOptions
                  , genericToJSON)
import Database.Bolt ((=:), props, query, queryP, queryP_)
import Data.Coerce (coerce)
import Data.List (intercalate)
import Data.List.NonEmpty (NonEmpty(..))
import Data.Semigroup (sconcat)
import Data.String.Interpolate (i)
import Data.Text (Text)
--import Debug.Trace (trace)
import GHC.Generics (Generic)


-- | Helper data type used to provide results of requests made to the
-- REST API
data ReqResult a
  = ReqResult { node :: NodeId -- ^ Node resulted by the request
              , result :: a -- ^ Result of the request
              }
  deriving (Generic, ToJSON)


loadRoot :: Label -> String -> App IO (Either String (ReqResult GoalId))
-- | Same as 'loadRootFormula', but in the 'App' monad, and with
-- result appropriate for the REST API
loadRoot label rootTxt
  = bolt
  $ fmap (uncurry ReqResult) . coerce
  <$> loadRootFormula label rootTxt


showRoots :: App IO [(RootId, Text, Text)]
-- | Return proofs available in the graph backend. The output is a
-- list of pairs (root identifier, formula string).
showRoots = boltTransact $ fmap go <$> query showProofsQuery
  where
    go r = (r =! "rootId", r =! "rootFormula", r =! "rootLabel")
    showProofsQuery =
      [i|MATCH (root:Root)-[:DerivedBy]->(n)-[:Derives]->(f:Formula)
RETURN id(root) AS rootId, f.formula as rootFormula, root.label AS rootLabel|]


checkProofs :: Maybe RootId -> App IO [String]
checkProofs Nothing = do
  roots <- fmap (\(rootId, _, _) -> rootId) <$> showRoots
  msum $ checkProofs.Just <$> roots
checkProofs (Just rootId) = boltTransact $ do
  r <- checkProof rootId
  case r of
    Ok{} -> return []
    Err (e:|es) -> return $ e:es


data CheckResult a = Err (NonEmpty String) | Ok a
  deriving (Eq, Show)

instance Semigroup (CheckResult a) where
  Ok{} <> Ok x = Ok x
  Ok{} <> e@Err{} = e
  e@Err{} <> Ok{} = e
  (Err es) <> (Err es') = Err (es <> es')

mkErr :: [String] -> Bolt (CheckResult a)
mkErr err = return $ Err (unwords err :| [])

checkProof :: RootId -> Bolt (CheckResult NodeId)
checkProof root = do
  crown <- getCrownFromRoot root
  -- TODO: check that this will fail fast (because of laziness? or do i need
  -- something like maybeT?)
  crownOk <- oneRelationship (initRelSpec crown){relType = Just "Meta"}
  rootOk <- oneRelationship
    $ RelationSpec (coerce root) (Just "DerivedBy") (Just "name") (Just "root")
  case crownOk <> rootOk of
    e@Err{} -> return e
    Ok derivedNode -> checkProofNode root derivedNode

data RelationSpec = RelationSpec { node :: NodeId, relType :: Maybe Text
                    , prop :: Maybe Text, propvalue :: Maybe Text
                    }

initRelSpec :: NodeId -> RelationSpec
initRelSpec node = RelationSpec node Nothing Nothing Nothing

oneRelationship :: RelationSpec -> Bolt (CheckResult NodeId)
oneRelationship RelationSpec{node, relType, prop, propvalue} = do
  targets <- oneResult "oneRelationship" (=! "targets") q ps
  case targets of
    [] -> mkErr ["No relationship with source", show node, "as specified:", optErrMsg]
    [t] -> return $ Ok t
    _ -> mkErr ["More than one relationship with source", show node, "as specified:", optErrMsg]
  where
    msgMaybe _ Nothing = ""
    msgMaybe msg (Just x) = msg ++ ":" ++ show x
    optErrMsg = intercalate "; " [msgMaybe "relationship type" relType]
    q = [i|OPTIONAL MATCH (m)-[r]->(n)
WHERE id(m) = $nodeId AND type(r) = $type AND ($prop IS NULL or r[$prop] IS NOT NULL) AND ($prop IS NULL OR $value IS NULL OR r[$prop] = $value)
WITH collect(id(n)) AS targets
RETURN targets|]
    ps = props ["nodeId" =: node, "type" =: relType, "prop" =: prop
               , "value" =: propvalue ]

checkProofNode :: RootId -> NodeId -> Bolt (CheckResult NodeId)
-- we carry over the root argument just to check that leaves and root point to the
-- same crown node
checkProofNode root node = do
  kind <- nodeKind node
  oneBuildOk <- oneRelationship (initRelSpec node){relType = Just "Derives"}
  nodeOk <- case kind of
    Leaf -> checkLeaf root node
    Goal -> mkErr ["Incomplete proof: proof with root", show root, "has goal node with ID", show node]
    Rule "ImpIntro" -> checkImpIntro root node
    Rule "ImpElim" -> checkImpElim root node
    Rule _ -> fail "Unknown rule name"
    Root -> failKind
    Crown -> failKind
    Formula -> failKind
  return $ oneBuildOk <> nodeOk
  where
    failKind = fail "Internal error: should only receive nodes representing rules, leaves, or goals."

data Builds = Ant | Csq deriving (Eq)

checkDerivesAs :: NodeId -> NodeId -> Builds -> Bolt (CheckResult NodeId)
-- check that the nodes derived by the input nodes are related by a Builds
-- relationship
checkDerivesAs n m label = do
  f <- derives n
  g <- derives m
  let ps = props [ "fId" =: f, "gId" =: g
                 , "name" =: case label of Ant -> "ant"; Csq -> "csq" :: Text]
  mBuilds <- maybeResult "derivesAs" (const ()) q ps
  case mBuilds of
    Nothing -> mkErr ["Node", show n, "does not derive the", case label of Ant -> "antecedent"; Csq -> "consequent", "of the derivation of", show m]
    Just{} -> return $ Ok n
  where
    q = [i|OPTIONAL MATCH (f)-[b:Builds]->(g)
WHERE id(f) = $fId AND id(g) = $gId AND b[$name] = true
RETURN true|]


checkLeaf :: RootId -> NodeId -> Bolt (CheckResult NodeId)
checkLeaf root leaf = do
  crownOk <- oneRelationship (initRelSpec leaf){relType=Just"Meta"}
  (discards, root') <- collectDiscardsAndRoot
  rootOk <- if root' == root
            then return (Ok $ coerce root)
            else mkErr ["Leaf", show leaf, "points to different root", show root', "than proof root", show root]
  case discards of
    [] -> mkErr ["No discard related to leaf", show leaf]
    _ -> do
      discardsOk <- mapM (\d -> checkDerivesAs leaf d Ant) discards
      return $ sconcat (crownOk:|(rootOk:discardsOk))
  where
    collectDiscardsAndRoot =
      oneResult "collectDiscards" (\r ->(r =! "discards", r =! "rootId")) q ps
      where
        q = [i|MATCH (l:Leaf)-[:Meta]->()-[:Meta]->(r:Root)
WHERE id(l) = $leafId
WITH l, r
MATCH (l)<-[:Hypothesis]-(p)
WITH collect(id(p)) AS discards, r
RETURN discards, id(r) AS rootId|]
        ps = props ["leafId" =: leaf]

checkImpIntro :: RootId -> NodeId -> Bolt (CheckResult NodeId)
checkImpIntro root ii = do
  derivedOk <- oneRelationship
               $ RelationSpec ii (Just "DerivedBy") (Just "name") (Just "pred")
  case derivedOk of
    Ok child -> do
      derivesOk <- checkDerivesAs child ii Csq
      childOk <- checkProofNode root child
      return $ derivesOk <> childOk
    e@Err{} -> return e

checkImpElim :: RootId -> NodeId -> Bolt (CheckResult NodeId)
checkImpElim root ie = do
  etorChildOk <- oneRelationship
    $ RelationSpec ie (Just "DerivedBy") (Just "name") (Just "etor")
  etedChildOk <- oneRelationship
    $ RelationSpec ie (Just "DerivedBy") (Just "name") (Just "eted")
  case (etorChildOk, etedChildOk) of
    (e@Err{}, e'@Err{}) -> return $ e <> e'
    (e@Err{}, _) -> return e
    (_, e@Err{}) -> return e
    (Ok etor, Ok eted) -> do
      etorDerivesOk <- checkDerivesAs etor ie Ant
      etedDerivesOk <- checkDerivesAs eted ie Csq
      etorOk <- checkProofNode root etor
      etedOk <- checkProofNode root eted
      return $ etorDerivesOk <> etedDerivesOk <> etorOk <> etedOk


data ProofNode -- | Data type for proof nodes
  = ProofNode { _id :: NodeId -- ^ node identifier
              , formula :: Text -- ^ subformula derived by this node
              , kind :: NodeKind -- ^ node kind
              , role :: Text -- ^ role played by this node (usually the rule name)
              , children :: Maybe [ProofNode] -- ^ optionally: children nodes
              }
  deriving (Generic, Show)

aesonOptions :: Options
aesonOptions = defaultOptions { omitNothingFields = True }

instance ToJSON ProofNode where
  toJSON = genericToJSON aesonOptions


leafOrGoal :: Maybe NodeId -> NodeKind
leafOrGoal = maybe Goal (const Leaf)


discardHypothesisInteractive :: GoalId -> Bolt (Maybe NodeId)
discardHypothesisInteractive goalId = do
  discards <- oneResult "getDiscards" (=! "discards") q ps
  case discards of
    [] -> return Nothing
    d:ds -> queryP_ rmHypsQ ps >> Just <$> discardHypothesisCloseBranch (Discard goalId (d:|ds))
  where
    ps = props ["goalId" =: goalId]
    q = [i|MATCH (goal:Goal)-[:Derives]->(ded)
WHERE id(goal)=$goalId
WITH goal, id(ded) AS dedId, size(goal.hyps) - 1 AS len
WITH [ix IN range(0, len) WHERE goal.hyps[ix] = dedId
  | goal.discards[ix]] AS discards, goal
RETURN discards|]
    rmHypsQ = [i|MATCH (goal:Goal)
WHERE id(goal) = $goalId
REMOVE goal.hyps, goal.discards|]


getFormula :: FormulaId -> Bolt Text
getFormula fId = oneResult "getFormula" (=! "derivesFormula") q params
  where
    params = props ["derives" =: fId]
    q = [i|MATCH (f:Formula) WHERE id(f) = $derives
RETURN f.formula AS derivesFormula|]


implicationIntroBackwardInteractive :: GoalId -> App IO (ReqResult ProofNode)
-- | Same as 'implicationIntroBackward', plus possibly discard
-- hypothesis; provide output useful for REST API server
implicationIntroBackwardInteractive goalId
  = boltTransact $ do
  IntroRes{rule,goal,derived,hypothesis} <- implicationIntroBackward goalId
  let ps = props ["goalId" =: goal, "ruleId" =: rule
                     , "goalDerivesId" =: derived, "hypothesisId" =: hypothesis
                     ]
  _ <- oneResult "IntroInsertHypothesis" (const ()) q ps
  derivesFormula <- getFormula derived
  maybeLeaf <- discardHypothesisInteractive goal
  return $ ReqResult rule (ProofNode { _id = coerce goal
                                     , formula = derivesFormula
                                     , kind = leafOrGoal maybeLeaf
                                     , role = "pred"
                                     , children = Nothing})
  where
    q = [i|MATCH (goal:Goal)
WHERE id(goal) = $goalId
// add hypothesis
WITH size(goal.hyps) AS len, goal
SET goal.hyps = goal.hyps + $hypothesisId
SET goal.discards = goal.discards + $ruleId
RETURN true|]


implicationElimBackwardInteractive :: GoalId -> FormulaId -> App IO (ReqResult (ProofNode, ProofNode))
-- | Same as 'implicationElimBackward', but also check for possible
-- hypothesis discards and return value useful for REST API server
implicationElimBackwardInteractive goalId etorId = boltTransact $ do
  ElimRes{rule, etedGoal, etedDerives, etorGoal, etorDerives} <-
    implicationElimBackward goalId etorId
  maybeEtedLeaf <- discardHypothesisInteractive etedGoal
  maybeEtorLeaf <- discardHypothesisInteractive etorGoal
  etedDerivesFormula <- getFormula etedDerives
  etorDerivesFormula <- getFormula etorDerives
  return $ ReqResult { node = rule
                     , result = (ProofNode { _id = coerce etedGoal
                                           , formula = etedDerivesFormula
                                           , kind = leafOrGoal maybeEtedLeaf
                                           , role = "eted"
                                           , children = Nothing}
                                , ProofNode { _id = coerce etorGoal
                                            , formula = etorDerivesFormula
                                            , kind = leafOrGoal maybeEtorLeaf
                                            , role = "etor"
                                            , children = Nothing})}


goalHypotheses :: GoalId -> App IO [(FormulaId, Text)]
-- | Return hypotheses available at goal specified by identifier
goalHypotheses goalId = bolt $ fmap go <$> queryP q params
  where
    params = props ["goalId" =: goalId]
    q = [i|MATCH (goal:Goal)
WHERE id(goal)=$goalId
UNWIND goal.hyps AS hypId
MATCH (hypothesis:Formula)
WHERE id(hypothesis) = hypId
RETURN hypId as hypothesisId, hypothesis.formula as hypothesisFormula|]
    go r = (r =! "hypothesisId", r =! "hypothesisFormula")


proofTree :: Int -> RootId -> App IO [ProofNode]
-- | @'proofTree' maxLevel rootId@: Return proof tree rooted at node
-- identified by @rootId@, up to @maxLevel@ levels. The root node can
-- be the root of any subproof, not necessarily the root of whole
-- proof.
proofTree maxLevel rootId = boltTransact $ go maxLevel (coerce rootId)
  where
    getNodeQuery =
      [i|MATCH (parent:Rule)-[db:DerivedBy]->(node)-[:Derives]->(f:Formula)
WHERE id(parent) = $parentId
RETURN id(node) AS nodeId, f.formula AS nodeFormula, db.name AS role|]
    go :: Int -> NodeId -> Bolt [ProofNode]
    go level nodeId = do
      let params = props ["parentId" =: nodeId]
      childrenParts <- fmap getNodeInfo <$> queryP getNodeQuery params
      childrenKinds <- mapM (nodeKind . (\(nId, _, _) -> nId)) childrenParts
      let children = zipWith makeNode childrenParts childrenKinds
      if level == 0
        then return children
        else mapM addChildren children
      where
        getNodeInfo r = (r =! "nodeId", r =! "nodeFormula", r =! "role")
        makeNode (nId, formula, role) kind =
          ProofNode nId formula kind role Nothing
        addChildren n@ProofNode{_id} = do
          children <- go (level - 1) _id
          return n{children = if null children then Nothing else Just children}


deleteProof :: RootId -> App IO Bool
-- | Delete proof with root in the specified identifier.
deleteProof rootId = boltTransact $ do
  r <- maybeResult "deleteCrown" (const ()) q ps
  case r of
    Just _ -> True <$ deleteSubproof (coerce rootId)
    Nothing -> return False
  where
    ps = props ["rootId" =: rootId]
    q = [i|MATCH (crown:Crown)-[:Meta]->(root:Root)-[:DerivedBy]->()-[:Derives]->(f:Formula)
WHERE id(root) = $rootId
DETACH DELETE crown
WITH f
MATCH (f)<-[:Builds*]-(node)
DETACH DELETE node
WITH count(node) AS m, f
DETACH DELETE f
RETURN m + 1 AS n|]


revertProof :: NodeId -> App IO GoalId
-- | Revert subproof rooted at node with the input identifier. This
-- reverses all the proof steps (rule applications) performed below
-- this node, and it is turned into a goal.
revertProof node = boltTransact $ do
  goal <- revertSubproof node
  (hyps, discards) <- collectHypotheses $ coerce goal
  let ps = props ["goalId" =: goal, "hyps" =: hyps, "discards" =: discards]
  _ <- oneResult "setHyps" (const ()) q ps
  return goal
  where
    q = [i|// collect hypotheses available for goal and set them (interactive only)
MATCH (g:Goal)
WHERE id(g) = $goalId
SET g.hyps = $hyps
SET g.discards = $discards
RETURN true|]
