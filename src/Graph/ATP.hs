{-|
Module      : Graph.ATP
Description : Automatic graph theorem prover
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declare functions attempting automatic proofs. All @solve*@ functions are limited
by two positive integers, one limiting the depth of the proof tree, and another
limiting the number of steps performed in the search. When one of these functions
receives another integer it is an identifier specifying part of a proof: a goal or
a root node. These are the identifiers used by the graph backend (Neo4j).
-}

-- INVARIANT: since it's DFS, all goals in the stack above a certain
-- level are related to the current branch
--- INVARIANT: competitions are a linked list, each competition tracks
--- the number of open branches until the next competition

{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE StrictData #-}
module Graph.ATP where

import Graph.Bolt ( (=!), Bolt, FormulaId(..), GoalId(..), Label, NodeId(..)
                  , IntroRes(..), ElimRes(..), HypothesisDiscard(..)
                  , bolt, collectHypotheses, derives, discardHypothesisCloseBranch
                  , implicationIntroBackward
                  , implicationElimBackward, loadFormula, maybeResult
                  , oneResult, possibleEliminators, revertSubproof
                  )
import Graph.Lib (App)
import Logic.MIMP (Formula)


import Control.Monad.State.Strict ( StateT(..), evalStateT, gets, lift
                                  , modify', state, when)
import Data.Coerce (coerce)
import Database.Bolt ((=:), props)
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as M
import Data.List ((\\))
import Data.List.NonEmpty (NonEmpty((:|)))
import qualified Data.List.NonEmpty as NE
import Data.Set (Set)
import qualified Data.Set as S
import Data.String.Interpolate (i)
import Data.Word (Word)
--import Debug.Trace (trace)

data S
  = S { stack :: [(Level, GoalId, FormulaId)] -- ^ stack of goals to explore
      , competitions :: [Competition] -- ^ stack of competitions in progress
      , hypotheses :: IntMap [NodeId] -- ^ hypotheses and where they are discarded
      -- | ‘memory’ used to prevent cycles (more appropriately named
      -- derived since last *unique* hypothesis)
      , derivedSinceLastHypothesis :: Set FormulaId
      --- OPTIMIZE: this could be a Data.Sequence, but then its size
      --- would be O(level); it could be an intmap whose values are
      --- the number of occurrences, so that we can incrementally
      --- change it when we change levels, instead of throwing it
      --- away. as a Set, we have no option but to create it anew
      --- whenever we close or give up on a branch
      , focus :: (Level, GoalId)
      , deadends :: [GoalId] -- ^ goals which that could not be proved
      } deriving (Show)

initS :: NonEmpty (Level, GoalId, FormulaId) -> S
initS ((l, g, f):|gs) = S ((l, g, f):gs) [] M.empty S.empty (l, g) []

hypothesisMap :: ([FormulaId], [NodeId]) -> IntMap [NodeId]
hypothesisMap (hyps, intros)
  = M.fromListWith (<>)
    $ zipWith (\(FormulaId hypId) intro -> (hypId, [intro])) hyps intros

type ATP = StateT S Bolt

type Limit = Word
type Level = Int
type Open = Word


data Competition
  = Comp { n :: Open -- ^ number of open branches
         , level :: Level -- ^ level where competition started
         , rule :: NodeId
         , competing :: [FormulaId] -- ^ eliminators in competition
         } deriving (Show)


compete :: Level -> NonEmpty FormulaId -> NodeId -> ATP ()
compete l etorIds ruleId = modify' go where
  go s@S{competitions} = s{competitions = competition:competitions}
  competition = Comp 1 -- impElim will add one to this
                l ruleId $ NE.toList etorIds


score :: GoalId -> NodeId -> ATP ()
-- ‘score’ a goal: discard hypothesis (using only one introduction for
-- efficiency), then attempt to close competitions
score goal introRule
  = lift (discardHypothesisCloseBranch d) -- discard hypothesis here
  >> modify' go
  where
    d = Discard goal (introRule:|[])
    -- no competitions, no problem
    go s@S{competitions = []} = s
    -- when competition has only one open branch we close it (we have
    -- a winner, ladies and gentlemen!) and then cascade
    go s@S{competitions = Comp{n=1}:cs} = go s{competitions=cs}
    -- close one branch of the current competition
    go s@S{competitions = c@Comp{n}:cs} = s{competitions=c{n=n-1}:cs}


branch :: ATP ()
-- keeps number of open branches in a competition correct
branch = modify' go where
  go s@S{competitions = c@Comp{n}:cs} = s{competitions=c{n=n+1}:cs}
  go s@S{competitions = []} = s


backtrack :: GoalId -> ATP ()
backtrack staleGoalId = do
  comps <- gets competitions
  case comps of
    --- TODO: when there's nothing left to do there's no point in
    --- attempting to prove other goals
    -- no competitions
    [] -> addDeadGoal
    -- there is at least one competition
    Comp{level, competing, rule}:_ -> do
      -- remove stale goals from the stack (those that are below [have
      -- a greater level than the] competition we are backtracking on)
      modify' (\s@S{stack} -> s{stack = removeStale level stack})
      case competing of
        -- no competitors left in the current competiton (we've tried
        -- all already), so we backtrack again
        [] -> dropCurrentCompetition >> backtrack staleGoalId
        e:es -> do -- there is at least one competitor left
          dropCurrentCompetition
          -- revert subproof below competition
          (goalId, hypotheses, intros) <- lift $ revertAndGetHyps rule
          -- update state:
          --- - reset derivations since last hypothesis
          _ <- setDerivedUntilLastHypothesis goalId
          --- - remove hypotheses that don't exist anymore
          -- TODO: for now we re-build them from scratch
          modify' (\s -> s{ hypotheses = hypothesisMap (coerce hypotheses, intros)
                          , focus = (level, goalId)})
          implicationElimBackwardAutomatic level goalId e es
  where
    -- remove current competition
    dropCurrentCompetition = modify' go'
      where
        go' s@S{competitions = []} = s
        go' s@S{competitions = _:cs} = s{competitions = cs}
    addDeadGoal = modify' (\s@S{deadends} -> s{deadends=staleGoalId:deadends})
    removeStale l = dropWhile (\(l',_,_) -> l' > l)
    revertAndGetHyps rule = do
      goal <- revertSubproof rule
      (hypotheses, discards) <- collectHypotheses $ coerce goal
      return (goal, hypotheses, discards)


addStack :: Level -> GoalId -> FormulaId -> ATP ()
addStack l g f = modify' (\s@S{stack} -> s{stack = (l, g, f):stack})


popStack :: ATP (Maybe (Level, GoalId, FormulaId))
popStack = state go where
  go s@S{stack = []} = (Nothing, s)
  go s@S{stack = g:gs} = (Just g, s{stack = gs})


remember :: FormulaId -> ATP ()
remember fId = modify' go where
  go s@S{derivedSinceLastHypothesis}
    = s{derivedSinceLastHypothesis = S.insert fId derivedSinceLastHypothesis}


addHypothesis :: FormulaId -> NodeId -> ATP ()
addHypothesis (FormulaId hypId) discard
  = modify' go where
  go s@S{hypotheses} =
    let (m, hyps)
          = M.insertLookupWithKey (\_ a b -> a <> b) hypId [discard] hypotheses
    in case m of
         Just (_:_) -> s{ hypotheses = hyps }
         -- if Nothing or Just [] hypothesis is newly-seen
         _ -> s{ hypotheses = hyps, derivedSinceLastHypothesis = mempty }


canCloseBranch :: FormulaId -> ATP (Maybe (NonEmpty NodeId))
canCloseBranch (FormulaId derivedId) = gets go where
  go S{hypotheses} = NE.nonEmpty $ M.findWithDefault [] derivedId hypotheses


implicationIntroBackwardAutomatic :: Level -> GoalId -> ATP ()
implicationIntroBackwardAutomatic l goalId = do
  IntroRes{rule, goal, derived, hypothesis}
     <- lift $ implicationIntroBackward goalId
  _ <- addHypothesis hypothesis rule
  mIntroRules <- canCloseBranch derived
  case mIntroRules of
    Just (ir:|_) -> score goal ir
    Nothing -> addStack (l+1) goal derived


implicationElimBackwardAutomatic :: Level -> GoalId -> FormulaId
  -> [FormulaId] -- ^ competitors in elimination
  -> ATP ()
implicationElimBackwardAutomatic l goalId elimId competitors = do
  ElimRes{rule, etedGoal, etedDerives, etorGoal, etorDerives}
    <- lift $ implicationElimBackward goalId elimId
  _ <- case competitors of
         [] -> return ()
         c:cs -> compete l (c:|cs) rule
  _ <- branch
  isEtedLeaf <- canCloseBranch etedDerives
  isEtorLeaf <- canCloseBranch etorDerives
  case (isEtedLeaf, isEtorLeaf) of
    (Just (etedIR:|_), Just (etorIR:|_))
      -> score etedGoal etedIR
       >> score etorGoal etorIR
    (Just (etedIR:|_), Nothing)
      -> score etedGoal etedIR
       >> stack etorGoal etorDerives
    (Nothing, Just (etorIR:|_))
      -> score etorGoal etorIR
       >> stack etedGoal etedDerives
    _ -> stack etedGoal etedDerives >> stack etorGoal etorDerives
  where
    stack = addStack (l+1)


data JustReachable = No | YesIntro | YesElim FormulaId
  deriving Show


goalJustReachable :: GoalId -> Bolt JustReachable
--- OPTIMIZE: run mayImpIntro instead of introReachable
-- OPTIMIZE: maybe don't check if its introReachable, since if it's
-- not elimReachable impIntro will be applied (on the other hand, if
-- it is introReachable, we wouldn't check for elimReachable…)
goalJustReachable goalId = do
  result <- maybeResult "introReachable" (const ()) reachableByIntro params
  case result of
    -- check if there was a result or not
    Nothing -> do
      maybeEtorId <- maybeResult "elimReachable" (=! "etorId") reachableByElim params
      return $ maybe No YesElim maybeEtorId
    Just _ -> return YesIntro
  where
    reachableByIntro =
      [i|MATCH (goal:Meta)-[:Derives]->(goalFormula:Formula)<-[:Builds {name: "csq"}]-(:Formula)-[:Builds {name: "ant"}]->(:Formula)<-[:Derives]-(:Rule {name: "impIntro"})-[:DerivedBy {name: "hyp"}]->(goal)
// goal is just reachable by implication introduction if its target
// formula’s consequent is the antecedent of an implication
// introduction
WHERE id(goal) = $goalId
RETURN true AS reachable
LIMIT 1|]
    reachableByElim =
      [i|// first get possible eliminator
MATCH (goal:Meta)-[:Derives]->(pred:Formula)-[:Builds {name: "csq"}]->(eted:Formula)<-[:Builds {name: "ant"}]-(etor:Formula)
WHERE id(goal) = $goalId
WITH goal, eted, etor
// check if is dischargeable
MATCH (goal:Meta)<-[:DerivedBy {name: "hyp"}]-(:Rule {name: "impIntro"})-[:Derives]->(:Formula)<-[:Builds {name: "ant"}]-(eted)
WITH goal, etor
MATCH (goal:Meta)<-[:DerivedBy {name: "hyp"}]-(:Rule {name: "impIntro"})-[:Derives]->(:Formula)<-[:Builds {name: "ant"}]-(etor)
RETURN id(etor) as etorId
LIMIT 1|]
    params = props ["goalId" =: goalId]


mayImpIntro :: GoalId -> Bolt Bool
-- | Return true if applying implication introduction rule is possible
-- and won't result in an immediate cycle
mayImpIntro goalId =
  oneResult "mayImpIntro" (=! "mayImpIntro") q params
  where
    q = [i|MATCH (goal:Goal)
WHERE id(goal) = $goalId
WITH goal
OPTIONAL MATCH (goal)-[:Derives]->(:Formula)<-[:Builds {csq: true}]-(g:Formula)
WITH goal, g
OPTIONAL MATCH (f:Formula)<-[:Derives]-(:Rule {name: "impElim"})-[:DerivedBy {name: "eted"}]->(goal)
RETURN g IS NOT NULL AND (f IS NULL OR id(f) <> id(g)) AS mayImpIntro|]
    params = props ["goalId" =: goalId]


step :: Level -> GoalId -> FormulaId -> ATP ()
-- | apply rule
step l goalId formulaId = do
  ds <- gets derivedSinceLastHypothesis
  if formulaId `S.member` ds
    then backtrack goalId
    else do
    _ <- remember formulaId
    justReachable <- lift $ goalJustReachable goalId
    case justReachable of
      No -> do
        -- OPTIMIZE: the caching done with derivedSinceLastHypothesis
        -- already detects cycles, so we just need to know if this is
        -- atomic or not
        canImpIntro <- lift $ mayImpIntro goalId
        if canImpIntro
          then impIntro goalId
          else do
          -- OPTIMIZE: cache these
          elimIds <- lift $ fmap fst <$> possibleEliminators goalId
          case elimIds of
            [] -> backtrack goalId
            etorId:es ->
              implicationElimBackwardAutomatic l goalId etorId es
      YesIntro -> impIntro goalId
      YesElim etorId -> impElim etorId
  where
    -- DOUBT: will this always run if I make it have no arguments?
    impIntro = implicationIntroBackwardAutomatic l
    impElim etorId = implicationElimBackwardAutomatic l goalId etorId []


setDerivedUntilLastHypothesis :: GoalId -> ATP ()
-- OPTIMIZE: might be better to do this incrementally, using the level
-- of the input goal, see comments at S data type
setDerivedUntilLastHypothesis goal = do
  ds <- lift $ unfold collectParentDerived goal
  modify' (\s -> s{derivedSinceLastHypothesis = S.fromList ds})
  where
    collectParentDerived node =
      maybeResult "getDerived" (\r -> (r =! "parent", r =! "derived"))
        q (props ["nodeId" =: node])
    unfold :: (a -> Bolt (Maybe (a, b))) -> a -> Bolt [b]
    unfold g n = do
      mr <- g n
      case mr of
        Just (p, f) -> (:) f <$> unfold g p
        Nothing -> return []
    q = [i|MATCH (n)<-[:DerivedBy]-(p)-[:Derives]->(f)
WHERE id(n) = $nodeId AND NOT "ImpIntro" IN labels(p)
RETURN id(f) AS derived, id(p) AS parent|]

removeHypothesesUpTo :: NodeId -> Level -> ATP ()
-- go up node levels, collecting the hypothesis that were introduced in the way,
-- then remove them from the state
removeHypothesesUpTo node levels = do
  res <- lift $ oneResult "updateHypothesis"
                    (\r -> (r =! "hypotheses", r =! "introductions"))
                    q
                    (props ["childId" =: node])
  let toRemove = hypothesisMap res
  modify' (\s@S{hypotheses}
           -> s{hypotheses = M.unionWith (\\) hypotheses toRemove})
  where
    -- OPTIMIZE: we can potentially do this and derivedUntilLastHypothesis in one
    -- pass
    q = [i|MATCH (child)
WHERE id(child) = $childId
MATCH (child)<-[:DerivedBy*0..#{levels}]-(intro:ImpIntro)
OPTIONAL MATCH (intro)-[:Derives]->()<-[:Builds {ant: true}]-(h)
WITH collect(id(intro)) AS introductions,
     collect(id(h)) AS hypotheses
RETURN hypotheses, introductions|]

-- | Data type for result of automatic proof attempt
data Result
  = Solved -- ^ Proof was found
  | ReachedMaxSteps -- ^ Could not find proof under maximum number of steps
  | ReachedMaxLevel -- ^ Could not find proof under maximum level
  | Unsolved [GoalId] -- ^ Could not find proof: there are remaining goals
  deriving (Eq, Show)


solve :: Level -> Limit -> ATP Result
solve maxLevel maxSteps = go 0 where
  go s
    | s >= maxSteps = noDeadEnds ReachedMaxSteps
  go s = popStack >>= \case
    Nothing -> noDeadEnds Solved
    Just (l, g, f)
      | l < maxLevel -> do
          -- NOTE: the condition prevLevel > l should only happen when the previous
          -- goal has become a leaf or is stale, hence the argument name prevNode
          -- and not prevGoal
          (prevLevel, prevNode) <- gets focus
          when (prevLevel > l) (up (prevLevel - l) g prevNode)
          modify' (\st -> st{focus = (l, g)})
          _ <- step l g f
          go (s+1)
      -- OPTIMIZE: maybe we want to save these goals somehow and come
      -- back to them later
      | otherwise -> noDeadEnds ReachedMaxLevel
  noDeadEnds :: Result -> ATP Result
  noDeadEnds r = do
    deadends <- gets deadends
    if null deadends then return r else return $ Unsolved deadends
  up d goal prevNode = do
    _ <- setDerivedUntilLastHypothesis goal
    removeHypothesesUpTo (coerce prevNode) d


solveGoal :: Level -> Limit -> GoalId -> App IO Result
solveGoal maxLevel maxSteps goal = bolt $ do
  d <- derives $ coerce goal
  evalStateT (solve maxLevel maxSteps) . initS $ (0, goal, d):|[]


solveFormula :: Level -> Limit -> Label -> Formula -> App IO Result
-- | Attempt proof of formula
solveFormula maxLevel maxSteps label rootFormula = do
  (_, rootGoal) <- bolt $ loadFormula label rootFormula
  solveGoal maxLevel maxSteps rootGoal



