{-|
Module      : Graph.Bolt
Description : Interact with graph backend
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declare functions interacting with the proof backend. Depends heavily
on the "Database.Bolt" module from the @hasbolt@ library.
-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE QuasiQuotes #-}

module Graph.Bolt (
  (=!),
  Bolt,
  BoltCfg(..),
  -- * Identifiers for graph nodes
  --
  -- | Every node and relationship in the graph backend has an
  -- identifier. All identifiers are integers, but creating @newtype@s
  -- for them makes the code safer and easier to document.
  FormulaId(..),
  GoalId(..),
  Label,
  NodeId(..),
  NodeKind(..),
  RootId(..),
  -- * Data types for results of rule applications
  IntroRes(..),
  ElimRes(..),
  HypothesisDiscard(..),
  -- * Open goals
  --
  -- | The three functions below have the same output type, differing
  -- only by their input
  remainingGoals,
  remainingGoalsByLeafOrGoal,
  -- * Other
  --
  -- | Other functions provided by the module
  bolt,
  boltTransact,
  collectHypotheses,
  clearGraph,
  deleteSubproof,
  derives,
  discardHypothesisCloseBranch,
  getCrownFromRoot,
  goalTypeCrown,
  graphStats,
  implicationElimBackward,
  implicationIntroBackward,
  loadFormula,
  loadRootFormula,
  maybeResult,
  nodeKind,
  oneResult,
  possibleEliminators,
  revertSubproof,
  testBoltConnection,
  ) where

import Control.Exception (try)
import Control.Monad (unless)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Reader (asks)
import Data.Aeson ( ToJSON(..))
import qualified Data.Aeson as A
import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NE
import Data.Map (Map)
import qualified Data.Map.Strict as M
import Data.Maybe (fromJust)
import Data.String.Interpolate (i)
import Data.Text (Text)
import qualified Data.Text as T
import Database.Bolt hiding (Node)
--import Debug.Trace (trace)
import GHC.Generics (Generic)
import Network.Connection (HostCannotConnect)
import Servant (FromHttpApiData)


import Logic.MIMP (Formula, Formula'(..), parse)
import Graph.Lib (App, Config(..))

type Bolt = BoltActionT IO

-- for pretty-printing bolt values (e.g., statistics we get from a
-- cypher query)
deriving instance Generic Value
instance ToJSON Structure where
  toJSON _ = A.String "<Structure>"
deriving instance ToJSON Value


type Label = String -- proof labels (sometimes formulas are too big!)
type Query = Text
type Params = Map Text Value

type NodeId' = Int
-- | general identifier, for any kind of node
newtype NodeId = NodeId NodeId'
  deriving newtype (Eq, FromHttpApiData, IsValue, Show, ToJSON)
-- | identifier for goal nodes
newtype GoalId = GoalId NodeId'
  deriving newtype (Eq, FromHttpApiData, IsValue, Read, Show, ToJSON)
-- | Identifier for root nodes
newtype RootId = RootId NodeId'
  deriving newtype (Eq, FromHttpApiData, IsValue, Read, Show, ToJSON)
-- | Identifier for formula nodes
newtype FormulaId = FormulaId NodeId'
  deriving newtype (Eq, FromHttpApiData, IsValue, Ord, Show, ToJSON)

instance IsValue a => IsValue (NonEmpty a) where
  toValue = toValueList . NE.toList
instance IsValue a => IsValue (Maybe a) where
  toValue Nothing = N ()
  toValue (Just x) = toValue x

instance RecordValue NodeId where
  exact (I nid) = pure $ NodeId nid
  exact x     = fail $ show x ++ " is not a NodeId value"
instance RecordValue GoalId where
  exact (I gid) = pure $ GoalId gid
  exact x     = fail $ show x ++ " is not a GoalId value"
instance RecordValue RootId where
  exact (I rid) = pure $ RootId rid
  exact x     = fail $ show x ++ " is not a RootId value"
instance RecordValue FormulaId where
  exact (I fid) = pure $ FormulaId fid
  exact x       = fail $ show x ++ " is not a FormulaId value"

-- | Kinds of proof nodes
data NodeKind = Leaf | Rule Text | Goal | Root | Crown | Formula
  deriving (Eq, Show)

-- SIGH: this is so fragile…
instance RecordValue NodeKind where
  exact (T "Leaf") = pure Leaf
  exact (T "Goal") = pure Goal
  exact (T "Root") = pure Root
  exact (T "Crown") = pure Crown
  exact (T "Formula") = pure Formula
  exact (T t) = pure $ Rule t
  exact x = fail $ show x ++ " is not a valid node label"
instance ToJSON NodeKind where
  toJSON Leaf = "Leaf"
  toJSON (Rule rule) = toJSON rule
  toJSON Goal = "Goal"
  toJSON Root = "Root"
  toJSON Crown = "Crown"
  toJSON Formula = "Formula"

oneResult :: String -> (Record -> a) -> Query -> Params -> Bolt a
-- | Helper function for performing queries over the BOLT protocol
-- that only return one result row
oneResult name f queryTxt params = do
  result <- maybeResult name f queryTxt params
  case result of
    Nothing -> fail $ unwords [ "Internal error: query"
                              , name
                              , "with params"
                              , show $ M.toList params
                              , "must have exactly one result"]
    Just r -> return r


maybeResult :: String -> (Record -> a) -> Query -> Params -> Bolt (Maybe a)
-- | Helper function for performing queries over the BOLT protocol
-- that return at most one result row
maybeResult name f queryTxt params = do
  rs <- queryP queryTxt params
  case rs of
    [] -> return Nothing
    [r] -> return . Just $ f r
    _ -> fail $ unwords [ "Internal error: query"
                        , name
                        , "with params"
                        , show $ M.toList params
                        , "shouldn't have more than one result"]


formulaNodesQuery :: RootId -> Formula -> Text
-- | return query that inserts root formula in graph
formulaNodesQuery (RootId rootId) = T.intercalate "\nUNION ALL\n" . go
  --- we use rootId to differentiate the formula nodes from one graph
  --- from those of another
  -- OPTIMIZE: might be able to optimize with
  -- https://neo4j.com/docs/cypher-manual/current/clauses/call-subquery/
  -- or maybe just use loadcsv…
  where
    go :: Formula -> [Text]
    go (Atom _) = []
    go n@(Impl left right)
      = [i|MERGE (l:Formula{formula: "#{left}", root: #{rootId}})
MERGE (r:Formula{formula: "#{right}", root: #{rootId}})
MERGE (n:Formula{formula: "#{n}", root: #{rootId}})
MERGE (l)-[:Builds{ant: true}]->(n)<-[:Builds{csq: true}]-(r)
RETURN n|] : concatMap go [left, right]


loadFormula :: Label -> Formula -> Bolt (RootId, GoalId)
-- | Load formula into graph backend, return the identifiers of the
-- root node and the formula goal.
--
-- Every proof graph is composed of formula nodes and derivation
-- nodes; every derivation notes points to the formula node that it
-- derives. 'loadFormula' creates the formula nodes and the
-- scaffolding for the derivation nodes to be created as the proof is
-- carried out.
loadFormula label rootFormula = transact $ do
  (rootId, goalId) <- oneResult "createRoot"
                      (\r -> (r =! "rootId", r =! "goalId"))
                      createRootQuery params
  unless (isAtomic rootFormula) $
         query_ $ formulaNodesQuery rootId rootFormula
  return (rootId, goalId)
  where
    isAtomic (Atom _) = True
    isAtomic _ = False
    createRootQuery = [i|CREATE (crown:Crown)-[:Meta]->(root:Rule:Root {label: $label})-[:DerivedBy {name: "root"}]->(g:Goal {hyps: [], discards: []})-[:Meta]->(crown)
CREATE (n:Formula {formula: $rootFormula})
SET n.root = id(root)
CREATE (g)-[:Derives]->(n)
RETURN id(root) AS rootId, id(g) AS goalId|]
    params = props ["rootFormula" =: show rootFormula, "label" =: label]

loadRootFormula :: Label -> String -> Bolt (Either String (RootId, GoalId))
-- | Same as 'loadFormula', but takes formula as a string, parses it,
-- and either reports errors or passes the result formula on to
-- 'loadFormula'
loadRootFormula label
  = either (return . Left) (fmap Right . loadFormula label) . parse


derives :: NodeId -> Bolt FormulaId
derives node
  = oneResult "derives" (=! "derives") q
              $ props ["nodeId" =: node]
  where
    q = [i|MATCH (n)-[:Derives]->(f) WHERE id(n) = $nodeId
RETURN id(f) AS derives|]


(=!) :: RecordValue a => Record -> Text -> a
-- | Helper function to extract value from record
(=!) r key = fromJust . exact $ r M.! key

data IntroRes
  = IntroRes { rule :: NodeId
             , goal :: GoalId
             , derived :: FormulaId
             , hypothesis :: FormulaId
             }

implicationIntroBackward :: GoalId
  -> Bolt IntroRes
-- | Apply backward implication introduction rule to user-provided
-- node ID
implicationIntroBackward goalId
  = oneResult "implicationIntroBackward" go q params
  where
    params = props ["goalId" =: goalId]
    q = [i|// matches any node which is an implication formula and is a goal
MATCH (prev:Rule)-[m:DerivedBy]->(goal)-[r:Derives]->(ded:Formula)<-[Builds {ant: true}]-(hyp:Formula)
WHERE id(goal) = $goalId
// introduce implication node connected to previous derivation node
CREATE (prev)-[m_:DerivedBy]->(n:Rule:ImpIntro {name: "impIntro"})-[r_:Derives]->(ded)
// set properties of new relationships to those of old ones
SET m_ = properties(m)
SET r_ = properties(r)
WITH ded, goal, hyp, m, n, r
DELETE m, r
WITH ded, n, hyp, goal
// get formula conclusion
MATCH (ded)<-[:Builds {csq: true}]-(conc:Formula)
// create new goal
CREATE (n)-[:DerivedBy {name: "pred"}]->(goal)-[:Derives]->(conc)
RETURN id(n) AS ruleId, $goalId AS goalId, id(conc) AS goalDerivesId, id(hyp) AS hypothesisId|]
    go r = IntroRes (r =! "ruleId") (r =! "goalId") (r =! "goalDerivesId")
                    (r =! "hypothesisId")


data ElimRes
  = ElimRes { rule :: NodeId
            , etedGoal :: GoalId
            , etedDerives :: FormulaId
            , etorGoal :: GoalId
            , etorDerives :: FormulaId
            }

implicationElimBackward :: GoalId -> FormulaId -> Bolt ElimRes
-- | Apply backward implication elimination rule to user-provided node
-- ID, using input hypothesis
implicationElimBackward goalId etorId
  = do
  (ruleId, dedId :: NodeId) <- oneResult "implicationElimBackwardCreation" (\r -> (r =! "ruleId", r =! "dedId")) createQ params
  etedGoal <- cloneGoal
  let params' = props ["etorGoalId" =: goalId, "etedGoalId" =: etedGoal
                      , "dedId" =: dedId, "etorId" =: etorId, "ruleId" =: ruleId]
  (etorDerives, etedDerives) <- oneResult "implicationElimBackwardAttachGoals"
    (\r -> (r =! "etorDerivesId", r =! "etedDerivesId")) attachQ params'
  return $ ElimRes ruleId etedGoal etedDerives goalId etorDerives
  where
    params = props
             [ "goalId" =: goalId
             , "etorId" =: etorId
             ]
    createQ = [i|// matches correct goal node
MATCH (prev:Rule)-[m:DerivedBy]->(goal:Goal)-[r:Derives]->(ded:Formula)
WHERE id(goal)=$goalId
// create derivation node pointing to deduced formula
CREATE (prev)-[m_:DerivedBy]->(impElim:Rule:ImpElim {name: "impElim"})-[r_:Derives]->(ded)
SET m_ = m
SET r_ = r
WITH ded, impElim, m, r
DELETE m, r
RETURN id(impElim) AS ruleId, id(ded) AS dedId|]
    attachQ = [i|// match goals
MATCH (etedGoal:Goal)
WHERE id(etedGoal) = $etedGoalId
WITH etedGoal
MATCH (etorGoal:Goal)
WHERE id(etorGoal) = $etorGoalId
WITH etedGoal, etorGoal
MATCH (impElim:Rule)
WHERE id(impElim) = $ruleId
WITH etedGoal, etorGoal, impElim
// deduced formula is the consequent of the eliminated formula
MATCH (etor:Formula)-[:Builds {ant: true}]->(eted:Formula)<-[:Builds {csq: true}]-(ded)
WHERE id(etor) = $etorId AND id(ded) = $dedId
// create links from goals to rule node
CREATE (impElim)-[:DerivedBy {name: "eted"}]->(etedGoal)-[:Derives]->(eted)
CREATE (impElim)-[:DerivedBy {name: "etor"}]->(etorGoal)-[:Derives]->(etor)
RETURN id(eted) as etedDerivesId, id(etor) as etorDerivesId|]
    -- we're cloning the goal in the middle of the implication elimination, when
    -- some relationships that should normally exist actually don't
    cloneGoal = oneResult "cloneGoal" (=! "cloneId") cloneQ params
      where
        -- because we don't use apoc we have to copy all types manually, since we
        -- can't set a relationship type dynamically (through a variable or
        -- parameter)
        cloneQ = [i|MATCH (goal)-[m:Meta]->(crown)
WHERE id(goal) = $goalId
CREATE (clone:Goal)-[m_:Meta]->(crown)
// copy properties
SET clone = properties(goal)
SET m_ = properties(m)
RETURN id(clone) AS cloneId|]


possibleEliminators :: GoalId -> Bolt [(FormulaId, Text)]
-- OPTIMIZE: this will return all antecedents of the formula deduced
-- by the goal, which might be a lot; so we would like to stream the
-- results, or return them in chunks (the interactive interface would
-- show only some, while the automatic one would consume the chunks,
-- try to prove them, and ask for more if none end up working)
-- | Return all possible eliminators for goal identifier. Returns list
-- of pairs (formula node id, formula string)
possibleEliminators goalId = fmap go <$> queryP theQuery params
  where
    params = props ["goalId" =: goalId]
    go r = (r =! "etorId", r =! "etor")
    theQuery =
      [i|MATCH (g:Goal)-[:Derives]->(pred:Formula)-[:Builds {csq: true}]->(eted:Formula)<-[:Builds {ant: true}]-(etor:Formula)
WHERE id(g)=$goalId
RETURN etor.formula AS etor, id(etor) AS etorId|]


collectHypotheses :: NodeId -> Bolt ([NodeId], [NodeId])
collectHypotheses node = oneResult "collectHypotheses" g q ps
  where
    g r = (r =! "hyps", r =! "discards")
    q = [i|MATCH (n)
WHERE id(n) = $nodeId
MATCH (p:ImpIntro)-[:DerivedBy*]->(n)
MATCH (p)-[:Derives]->(:Formula)<-[:Builds {ant: true}]-(hyp:Formula)
WITH collect(id(hyp)) AS hyps, collect(id(p)) AS discards
RETURN hyps, discards|]
    ps = props ["nodeId" =: node]


data HypothesisDiscard = Discard GoalId (NonEmpty NodeId)


discardHypothesisCloseBranch :: HypothesisDiscard -> Bolt NodeId
-- does not check if discard makes sense, so be careful!
discardHypothesisCloseBranch (Discard goalId introRules)
  = oneResult "discardHypothesis" (=! "leafId")
              q params
  where
    params = props [ "goalId" =: goalId
                   , "discards" =: introRules
                   ]
  -- perform hypothesis discard. creates hypothesis edges from rule applications
  -- where discards occurred to goal, and turns goal into a leaf. does not check if
  -- discards make sense. discards are lists of pairs where first element is
  -- hypothesis (formula) id and second is rule application id
    q = [i|MATCH (leaf:Goal)
WHERE id(leaf) = $goalId
UNWIND $discards AS impIntroId
MATCH (impIntro:Rule)
WHERE id(impIntro) = impIntroId
CREATE (impIntro)-[h:Hypothesis]->(leaf)
WITH collect(h) AS _, leaf
REMOVE leaf:Goal
SET leaf:Leaf
RETURN id(leaf) AS leafId|]


remainingGoals :: NodeId -> Bolt [(GoalId, FormulaId)]
-- | Return open goals attached to crown with the input node
-- identifier. Note that some of these goals may not need to be closed
-- if there is a competition between branches.
remainingGoals crownId
  = fmap (\r -> (r =! "goalId", r =! "derivesId"))
  <$> queryP getGoals params
  where
    params = props ["crownId" =: crownId]
    getGoals =
      [i|MATCH (crown:Crown)
WHERE id(crown) = $crownId
WITH crown
MATCH (f:Formula)<-[:Derives]-(goal:Goal)-[:Meta]->(crown)
RETURN id(goal) AS goalId, id(f) AS derivesId|]


remainingGoalsByLeafOrGoal :: NodeId -> Bolt [(GoalId, FormulaId)]
-- this function may be called with nodes that are not leaves nor
-- goals, in this case the return is []
-- | Return open goals in the proof to which the input identifier
-- belongs.
remainingGoalsByLeafOrGoal leafOrGoalId = do
  maybeCrownId <- maybeResult "getCrownFromLeafOrGoal" (=! "crownId")
                              getCrownFromGoalLeaf params
  case maybeCrownId of
    Nothing -> return []
    Just crownId -> remainingGoals crownId
  where
    params = props ["leafOrGoalId" =: leafOrGoalId]
    getCrownFromGoalLeaf =
      [i|MATCH (leafOrGoal)-[:Meta]->(crown:Crown)
WHERE id(leafOrGoal) = $leafOrGoalId AND head(labels(leafOrGoal)) IN ["Leaf", "Goal"]
RETURN id(crown) AS crownId|]


getCrownFromRoot :: RootId -> Bolt NodeId
getCrownFromRoot root
  = oneResult "getCrownFromRoot" (=! "crownId") q ps
  where
    q = [i|MATCH (root:Root)<-[:Meta]-(crown:Crown)
WHERE id(root) = $rootId
RETURN id(crown) AS crownId|]
    ps = props ["rootId" =: root]


goalTypeCrown :: GoalId -> Bolt (NodeId, Bool)
-- return id of crown node, plus if the goal is directly under the
-- root (we use this in the ATP module, because we apply different
-- techniques to regular goals and those under the root)
-- | Return identifier of crown node and boolean indicating if the
-- input goal identifier is that of a root goal.
--
-- The crown node is connected to all goals, leaves, and root of the
-- proof.
goalTypeCrown goalId = oneResult "goalTypeCrown" (\r -> (r =! "crownId", r =! "isRoot")) theQuery params
  where
    theQuery =
      [i|MATCH (goal:Goal)-[:Meta]->(crown:Crown)
WHERE id(goal) = $goalId
WITH crown, goal
OPTIONAL MATCH (root:Root)-->(goal)
RETURN id(crown) as crownId, root IS NOT NULL AS isRoot|]
    params = props ["goalId" =: goalId]

nodeKind :: NodeId -> Bolt NodeKind
nodeKind node = oneResult "nodeKindLabel" (=! "kind") q ps
  where
    q = [i|MATCH (n)
WHERE id(n) = $nodeId
WITH labels(n) as ls
RETURN CASE
WHEN "Root" IN ls THEN "Root"
WHEN "ImpIntro" IN ls THEN "ImpIntro"
WHEN "ImpElim" IN ls THEN "ImpElim"
WHEN "Goal" IN ls THEN "Goal"
WHEN "Leaf" IN ls THEN "Leaf"
WHEN "Crown" IN ls THEN "Crown"
WHEN "Formula" IN ls THEN "Formula"
END AS kind|]
    ps = props ["nodeId" =: node]


getCrown :: NodeId -> Bolt NodeId
getCrown node = oneResult "getCrown" (=! "crownId") q ps
  where
    q = [i|MATCH (crown:Crown)-[:Meta]->(:Root)-[:DerivedBy*]->(n)
WHERE id(n) = $nodeId
RETURN id(crown) AS crownId
LIMIT 1|]
    ps = props ["nodeId" =: node]

revertSubproof :: NodeId -> Bolt GoalId
revertSubproof node = do
  crown <- getCrown node
  (goal, rootToDelete) <- oneResult "revertSubproof" g q ps
  _ <- deleteSubproof rootToDelete
  let ps' = props ["goalId" =: goal, "crownId" =: crown]
  _ <- oneResult "linkWithCrown" id linkQ ps'
  return goal
  where
    g r = (r =! "goalId", r =! "root")
    ps = props ["nodeId" =: node]
    q = [i|// match parent/root of subproof, immediate child, and relationships
MATCH (p)-[db:DerivedBy]->(n)-[ds:Derives]->(f)
WHERE id(n) = $nodeId
CREATE (p)-[db_:DerivedBy]->(g:Goal)-[ds_:Derives]->(f) // create goal
// copy relationships from immediate child to goal, and delete old ones
SET ds_ = ds
SET db_ = db
WITH db, ds, g, n
DELETE db, ds
WITH g, n
RETURN id(g) AS goalId, id(n) AS root|]
    linkQ = [i|// link with crown
MATCH (g), (crown)
WHERE id(g) = $goalId AND id(crown) = $crownId
CREATE (g)-[:Meta]->(crown)
RETURN true|]


deleteSubproof :: NodeId -> Bolt Int
deleteSubproof node = oneResult "deleteSubproof" (=! "n") q ps
  where
    ps = props ["parentId" =: node]
    q = [i|MATCH (p)-[:DerivedBy*]->(node)
WHERE id(p) = $parentId
DETACH DELETE node
WITH count(node) AS n, p
DETACH DELETE p
RETURN n|]

clearGraph :: App IO ()
-- | Delete everything in the graph backend.
clearGraph
  = boltTransact
  $ query_ "MATCH (n) DETACH DELETE n"


graphStats :: App IO (Map Text Value)
-- | Return statistics about the graph backend.
graphStats
  = boltTransact
    $ oneResult "graph-statistics" (=! "stats")
    [i|CALL db.stats.retrieve("GRAPH COUNTS") YIELD data
RETURN data AS stats|] mempty


testBoltConnection :: App IO ()
-- | Test BOLT protocol connection to graph backend.
testBoltConnection = bolt $ query_ "RETURN true"


bolt :: Bolt a -> App IO a
-- | Lift 'BoltActionT' monad to 'App' monad
bolt action = do
  boltCfg <- asks boltC
  pipeOrErr <- liftIO . try $ connect boltCfg
  case pipeOrErr of
    Right pipe
      -> do
      result <- liftIO $ run pipe action
      liftIO $ close pipe
      return result
    Left (_ex :: HostCannotConnect) -> error "can't connect to database."


boltTransact :: Bolt a -> App IO a
-- | Lift 'BoltActionT' monad to 'App' monad, performing the bolt
-- actions as a transaction
boltTransact = bolt . transact
