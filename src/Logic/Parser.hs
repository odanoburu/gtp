{-|
Module      : Logic.Parser
Description : Common parts of logic language parsers
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declares common functions and types for logic language parsers.
-}
module Logic.Parser where

import Control.Monad (void)
import Data.Bifunctor (bimap)
import Data.Char (isAlphaNum, isSpace)
import Data.Void (Void)
import Text.Megaparsec hiding (some, token, label)
import Text.Megaparsec.Char (space)
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void String

type NodeLabel = String
-- IDEA: when we need to generalize Formula (and node!) types, use
-- typeclasses

spaceConsumer :: Monad m => ParsecT Void String m ()
spaceConsumer = L.space spaces empty empty
  where
    spaces = void $ takeWhile1P Nothing isSpace

lexeme :: Parser a -> Parser a
lexeme = L.lexeme spaceConsumer

symbol :: String -> Parser String
symbol = L.symbol spaceConsumer

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")


atomLabel :: Parser String
-- | An atom label is a nonempty string without whitespace nor
-- parentheses
atomLabel = lexeme $ takeWhile1P Nothing isAlphaNum


parseLogic :: Parser a -> String -> Either String a
parseLogic p input
  = bimap errorBundlePretty id
  $ parse go "" input
  where
    go = space *> p <* eof
