{-|
Module      : Main
Description : gtp's CLI
Copyright   : (c) bruno cuconato, 2020
License     : BSD-3
Maintainer  : bcclaro+haskell@gmail.com
Stability   : experimental

Declares a CLI for gtp, which is mainly meant for administration. Most
users will want to use the web interface for creating proofs. After
installing gtp, @gtp --help@ will show an overview of the available
commands.
-}
module Main (main) where

import Graph.ATP (Level, Limit, solveFormula, solveGoal)
import Graph.ITP (checkProofs)
import Graph.Bolt ( GoalId(..), Label, RootId(..)
                  , bolt, clearGraph, graphStats, loadFormula
                  , testBoltConnection )
import Graph.Lib (logMsg, runApp)
import Graph.Reader (CheckHamiltonian(..), parseGraph, economicalHamiltonian)
import Graph.Server (LogLevel(..), gtpServe)
import Logic.Lib (Logic(..), onFormula)


import Control.Monad.IO.Class (liftIO)
import Data.Aeson.Encode.Pretty (encodePrettyToTextBuilder)
import Data.Text.Lazy.Builder (toLazyText)
import qualified Data.Text.Lazy.IO as TLIO
import Network.Wai.Handler.Warp (Port)
import Options.Applicative


data GTPCommand = GTPCommand (Maybe FilePath) GTPsub
  deriving (Eq, Show)

data GTPsub
  = Serve Port LogLevel
  | Solve Level Limit ToSolve
  | Load Input String
  | Clear
  | Stats
  | Check (Maybe RootId)
  | Hamiltonian CheckHamiltonian String
  deriving (Eq, Show)

data Input = Input Logic Label InputKind deriving (Eq, Show)
data InputKind = File | Formula deriving (Eq, Show)
data ToSolve = Unloaded Input String | Goal GoalId deriving (Eq,Show)

showHelpOnErrorExecParser :: ParserInfo a -> IO a
showHelpOnErrorExecParser = customExecParser (prefs showHelpOnError)


parseSubCommand :: Parser GTPsub -> Parser GTPCommand
parseSubCommand subcommand = GTPCommand <$> configDir <*> subcommand
  where
    configDir = option (Just <$> str)
      (metavar "CONFIGDIR" <> short 'c' <> long "config-dir"
       <> help "Directory where configuration files are in"
       <> value Nothing)


subcommands :: (Parser a -> Parser b) -> [(String, Parser a, String)] -> Parser b
subcommands f = hsubparser . foldMap raise
  where
    raise (name, p, desc) = command name (info (f p) (fullDesc <> progDesc desc))

parseCommand :: Parser GTPCommand
parseCommand = subcommands parseSubCommand
  [ ("serve", serveP, "Start proof server (make a request for docs/ to see documentation)")
  , ("load", loadP, "Load formula into graph store")
  , ("solve", solveP, "Try to automatically solve a formula")
  , ("clear", pure Clear, "Clear stored graph. All data will be lost")
  , ("stats", pure Stats, "Print statistics about stored graph")
  , ("check", checkP, "Check validity of proofs")
  , ("hamiltonian", hamP, "Produce formula checking for presence (default) or absence of a hamiltonian path in GRAPH")]
  where
    serveP = Serve <$> portP <*> logLevelP
      where
        portP = option auto (metavar "PORT" <> short 'p' <> long "port"
                             <> help "Port to use" <> value 2487 <> showDefault)
        logLevelP = flag None Dev (long "log-devel" <> help "Log requests at development level" <> showDefault)
          <|> flag None Prod (long "log-prod" <> help "Log requests at production level" <> showDefault)
    loadP = Load <$> inputP <*> strArgument (metavar "INPUT")
    checkP = Check <$> option (Just . RootId <$> auto) (metavar "ROOT" <> short 'r' <> long "root"
                                   <> help "Root of proof to check (or none, to check all proofs)" <> value Nothing)
    hamP = Hamiltonian <$>
           (flag' Absence (short 'n' <> long "no-hamiltonian")
            <|> flag Presence Presence (short 'y' <> long "hamiltonian"))
           <*> strArgument (metavar "GRAPH"
                            <> help "Path to graph file")

inputP :: Parser Input
inputP = Input
  <$> option auto (long "logic"
                    <> short 'L'
                    <> value MinimalImplicational
                    <> help "Logic to use for INPUT (will embed more expressive logics into the minimal implicational fragment)")
  <*> strOption (long "label"
                  <> short 'n'
                  <> value ""
                  <> help "Label name for INPUT")
  <*> (file <|> formula)
  where
    file = flag' File (short 'f' <> long "file"
                       <> help "Take INPUT to be filename of where input formula is")
    formula = flag Formula Formula (short 'F' <> long "formula"
                                    <> help "Take INPUT to be a formula")

solveP :: Parser GTPsub
solveP = subcommands (\p -> Solve <$> levelP <*> stepsP <*> p)
  [ ("goal", goalP, "Try to prove GOAL")
  , ("formula", formulaP, "Prove INPUT")
  ]
  where
    levelP = option auto (metavar "LEVEL" <> short 'l' <> long "max-level"
                         <> help "Maximum depth of a branch in the proof attempt"
                         <> value 100 <> showDefault)
    stepsP = option auto (metavar "STEPS" <> short 's' <> long "max-steps"
                          <> help "Maximum number of steps to use in the proof attempt"
                         <> value 100 <> showDefault)
    goalP = Goal <$> argument auto (metavar "GOAL" <> help "Integer ID of goal to prove")
    formulaP = Unloaded <$> inputP <*> strArgument (metavar "INPUT")


gtpProgDesc :: String
gtpProgDesc =
  "Show and manipulate huge proofs"

gtpHeader :: String
gtpHeader = "gtp — graph theorem prover."

main :: IO ()
main = do
  (GTPCommand mConfigDir subcommand) <- showHelpOnErrorExecParser
    $ info (helper <*> parseCommand)
    (fullDesc <> progDesc gtpProgDesc <> header gtpHeader)
  runApp mConfigDir
    (case subcommand of
       Hamiltonian check graphFile -> do
         grOrErr <- liftIO $ parseGraph <$> readFile graphFile
         liftIO . putStrLn $ case grOrErr of
           Right gr -> show $ economicalHamiltonian check gr
           Left err -> err
       _ -> testBoltConnection *> case subcommand of
              Serve port logLevel
                -> gtpServe logLevel port
              Load what input -> testBoltConnection *> load what input
                                 >>= liftIO . printLoad
              Solve maxLevel maxSteps what -> solve maxLevel maxSteps what
                                              >>= liftIO . print
              Clear -> clearGraph
              Stats -> graphStats >>= liftIO . TLIO.putStrLn . toLazyText . encodePrettyToTextBuilder
              Check mRoot -> checkProofs mRoot >>= liftIO . mapM putStrLn
                >> return ()
              Hamiltonian{} -> error "Internal error: CLI fall-through")
    where
      load (Input logic label File) fp = liftIO (readFile fp)
        >>= load (Input logic label Formula)
      load (Input logic label Formula) rootFormula
        = onFormula logic rootFormula (either fail $ bolt . loadFormula label)
      printLoad :: (RootId, GoalId) -> IO ()
      printLoad (rootId, goalId) = logMsg $ unwords ["Root node has ID:", show rootId, "\nGoal node has ID:", show goalId]
      solve maxLevel maxSteps (Goal goalId) = solveGoal maxLevel maxSteps goalId
      solve maxLevel maxSteps (Unloaded (Input logic label Formula) rootFormula)
        = onFormula logic rootFormula (either fail $ solveFormula maxLevel maxSteps label)
      solve maxLevel maxSteps (Unloaded (Input logic label File) formulaFile)
        = liftIO (readFile formulaFile)
        >>= solve maxLevel maxSteps . Unloaded (Input logic label Formula)
