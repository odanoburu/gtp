module Graph.ATPSpec (spec) where

import Control.Monad.Reader (runReaderT)
import Test.Hspec

import Graph.BoltSpec (getConfig)
import Graph.Bolt (clearGraph, testBoltConnection)
import Graph.ATP (Result(..), Level, Limit, solveFormula)
import Graph.ITP (checkProofs)
import Graph.Lib (App)
import Logic.Lib (Logic(..), onFormula)


run :: App IO a -> IO a
run action = getConfig >>= runReaderT action

go :: Logic -> (Result -> Bool) -> Level -> Limit -> String -> IO Bool
go l f maxLevel maxSteps formula = run (onFormula l formula f')
  where
    f' (Left err) = fail err
    f' (Right x) = f <$> solveFormula maxLevel maxSteps "" x

isSolved :: Result -> Bool
isSolved Solved = True
isSolved _ = False

cantSolve :: Result -> Bool
cantSolve (Unsolved _) = True
cantSolve _ = False

needsMoreGas :: Result -> Bool
needsMoreGas ReachedMaxSteps = True
needsMoreGas _ = False

fibonacci3 :: String
fibonacci3 = "((1 -> 2) -> ((2 -> 3) -> (1 -> 3)))"

spec :: Spec
spec =
  beforeAll_ (run testBoltConnection)
    (describe "automatic proofs" $ do

      describe "Negative tests (does not prove)" $ do
        it "can't do anything without gas"
          $ go MinimalImplicational needsMoreGas 10 0 fibonacci3
          `shouldReturn` True

        it "can't do much without the necessary gas"
          $ go MinimalImplicational needsMoreGas 10 1 fibonacci3 `shouldReturn` True

        it "clears graph of incomplete proofs"
          $ run clearGraph `shouldReturn` ()

      describe "Positive tests (proves)" $ do
        it "solves simple formulas"
          $ go MinimalImplicational isSolved 10 2 "(1 -> 1)" `shouldReturn` True

        it "fibonacci formula n = 3"
          $ go MinimalImplicational isSolved 10 20 fibonacci3 `shouldReturn` True

        it "fibonacci n = 5"
          $ go MinimalImplicational isSolved 100 30 "(-> (-> 1 2) (-> (-> 1 (-> 2 3)) (-> (-> 2 (-> 3 4)) (-> (-> 3 (-> 4 5)) (-> 1 5)))))" `shouldReturn` True

        it "doesn't solve impossible formulas"
          $ go MinimalImplicational cantSolve 10 10 "(A1 -> A2)" `shouldReturn` True

        it "doesn't solve impossible formulas (1)"
          $ go MinimalImplicational cantSolve 10 10 "A1" `shouldReturn` True

      -- it "big fibonacci formula"
      --   $ (isSolved <$> (bolt $ solveFormula 50 $ fibonacci 11)) `shouldReturn` True

        it "solves Łukasiewicz 1"
          $ go MinimalImplicational isSolved 10 10 "(-> p (-> q p))" `shouldReturn` True

        it "solves Łukasiewicz 2"
          $ go MinimalImplicational isSolved 10 10 "(-> (-> p (-> q r)) (-> (-> p q) (-> p r)))" `shouldReturn` True

        it "solves embedded classical formulas and elim left"
          $ go Classical isSolved 10 10 "(-> (& 1 2) 1)" `shouldReturn` True

        it "solves embedded classical formulas ant elim right"
          $ go Classical isSolved 10 10 "(-> (& 1 2) 2)" `shouldReturn` True

        it "solves embedded classical formulas and intro"
          $ go Classical isSolved 100 20 "(-> 2 (-> 1 (& 1 2)))" `shouldReturn` True

        it "solves embedded classical formulas or elim"
          $ go Classical isSolved 100 30 "(-> (-> 1 3) (-> (-> 2 3) (-> (| 1 2) 3)))" `shouldReturn` True

        it "solves embedded classical formulas or intro left"
          $ go Classical isSolved 10 10 "(-> 1 (| 1 2))" `shouldReturn` True

        it "solves embedded classical formulas or intro right"
          $ go Classical isSolved 10 10 "(-> 2 (| 1 2))" `shouldReturn` True

        it "modus ponens"
          $ go Classical isSolved 100 20 "(-> (& (-> p q) p) q)"
          `shouldReturn` True

        -- it "modus tollens" -- a bit slow
        --   $ go Classical needsMoreGas 100 100 "(-> (& (-> p q) (~ q)) (~ p))"
        --   `shouldReturn` True

        it "hypothetical syllogism"
          $ go Classical isSolved 100 20 "(-> (& (-> p q) (-> q r)) (-> p r))"
          `shouldReturn` True

        -- it "disjunctive syllogism" -- kinda slow
        --   $ go Classical needsMoreGas 100 100 "(-> (& (| p q) (~ p)) q)"
        --   `shouldReturn` True

        -- it "destructive dilemma" -- not sure it stops
        --   $ go Classical needsMoreGas 100 100 "(-> (& (& (-> p q) (-> r s)) (| (~ q) (~ s))) (| (~ p) (~ r)))"
        --   `shouldReturn` True

        -- it "bidirectional dilemma" -- not sure it stops
        --   $ go Classical needsMoreGas 100 100 "(-> (& (-> p q) (& (-> r s) (| p (~ s)))) (| q (~ r)))"
        --   `shouldReturn` True

        it "composition"
          $ go Classical isSolved 100 100 "(-> (& (-> p q) (-> p r)) (-> p (& q r)))"
          `shouldReturn` True

        -- it "de morgan 1" -- kinda slow
        --   $ go Classical needsMoreGas 100 100 "(-> (~ (& p q)) (| (~ p) (~ q)))"
        --   `shouldReturn` True

        -- it "de morgan 2" -- kinda slow
        --   $ go Classical needsMoreGas 100 100 "(-> (~ (| p q)) (& (~ p) (~ q)))"
        --   `shouldReturn` True

        it "and-commutation"
          $ go Classical isSolved 100 30 "(-> (& p q) (& q p))"
          `shouldReturn` True

        it "and-association"
          $ go Classical isSolved 100 1000 "(-> (& p (& q r)) (& (& p q) r))"
          `shouldReturn` True

        it "or-commutation"
          $ go Classical isSolved 100 1000 "(-> (| p q) (| q p))"
          `shouldReturn` True

        -- it "double negation" -- kinda slow
        --   --- FIXME: proof of second element of conjunction does not seem to be
        --   --- deterministic in that sometimes it does not find a proof (Unsolved
        --   --- =/= maxStepsReached)
        --   $ go Classical needsMoreGas 100 100 "(& (-> p (~ (~ p))) (-> (~ (~ p)) p))"
        --   `shouldReturn` True


        -- it "transposition" -- kinda slow
        --   $ go Classical needsMoreGas 100 100 "(-> (-> p q) (-> (~ q) (~ p)))"
        --   `shouldReturn` True

        -- it "material implication" -- very slow
        --   $ go Classical needsMoreGas 100 100 "(-> (-> p q) (| (~ p) q))"
        --   `shouldReturn` True

        it "curry"
          $ go Classical isSolved 20 20 "(-> (-> (& p q) r) (-> p (-> q r)))"
          `shouldReturn` True

        it "uncurry"
          $ go Classical isSolved 20 20 "(-> (-> p (-> q r)) (-> (& p q) r))"
          `shouldReturn` True

        it "or tautology"
          $ go Classical isSolved 10 20 "(-> p (| p p))"
          `shouldReturn` True

        it "and tautology"
          $ go Classical isSolved 10 20 "(-> p (& p p))"
          `shouldReturn` True

        -- it "excluded middle" -- slow
        --   $ go Classical needsMoreGas 100 100 "(-> p (~ p))"
        --   `shouldReturn` True

        it "non-contradiction" -- slow, but only formula in the test spec using ~ (NOT)
          $ go Classical isSolved 100 100 "(~ (& p (~ p)))"
          `shouldReturn` True

        it "produces syntactically correct proofs"
          $ run (checkProofs Nothing) `shouldReturn` []

        it "clears graph alright"
          $ run clearGraph `shouldReturn` ()
    )
