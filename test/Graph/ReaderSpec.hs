{-# LANGUAGE QuasiQuotes #-}
module Graph.ReaderSpec (spec) where

import Test.Hspec
import Data.String.Interpolate (i)

import Graph.Reader (Graph(..), parseGraph)


spec :: Spec
spec =
  describe "formula parsing" $ do
    it "parses line with node with no edges" $
      parseGraph "1" `shouldBe` (Right $ Graph [(1, [])])

    it "parses line with node with edges" $
      parseGraph "1 2 4 5" `shouldBe` Right (Graph [(1, [2, 4, 5])])

    it "parses bigger graphs alright" $
      parseGraph [i| 1 2 3 4 
2 1
3  1 4
4 1  3 |] `shouldBe` Right (Graph [(1, [2,3,4]), (2, [1]), (3, [1,4]), (4, [1,3])])

