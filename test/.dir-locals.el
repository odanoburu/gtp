;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((haskell-mode
  (dante-target . "gtp:test:gtp-test")
  (compile-command . "stack build --fast --copy-bins --pedantic --test ")))
