module Logic.MIMPSpec (spec) where

import Test.Hspec

import Logic.MIMP (Formula'(..), parse)


spec :: Spec
spec =
  describe "formula parsing" $ do
    it "parses any group of alphanumeric characters as an atom" $
      parse "A1" `shouldBe` (Right $ Atom "A1")

    it "parses parenthesized infixed implications" $
      parse "(A1 -> A1)" `shouldBe` (Right $ Impl (Atom "A1") (Atom "A1"))

    it "parses bigger formulas alright" $
      parse "((A1 -> A2) -> ((A1 -> (A2 -> A3)) -> (A1 -> A3)))"
      `shouldBe` Right (Impl (Impl (Atom "A1") (Atom "A2"))
                         (Impl (Impl (Atom "A1") (Impl (Atom "A2") (Atom "A3")))
                           (Impl (Atom "A1") (Atom "A3"))))

    -- it "parses unparenthesized infixed implications associating to the right" $
    --   parse "A1 -> A2 -> A3" `shouldBe` (Right $ Impl (Atom "A1") (Impl (Atom "A2") (Atom "A3")))
      
